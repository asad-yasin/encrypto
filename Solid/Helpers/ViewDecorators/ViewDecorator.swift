//
//  ViewDecorator.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import Foundation
import UIKit
protocol ViewDecorator {
    func addCornerRadius(view: UIView,radius: CGFloat)
    func addBorderColor(view: UIView,color: UIColor?)
}
class UIViewDecorator: ViewDecorator {
    func addCornerRadius(view: UIView, radius: CGFloat) {
        view.layer.cornerRadius = radius
    }
    
    func addBorderColor(view: UIView, color: UIColor?) {
        guard let color = color else {
            return
        }
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = 1
    }
    deinit {
        print("ViewDecorator is deinit.")
    }
}
