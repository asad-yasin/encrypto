//
//  GestureHandler.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import Foundation
import UIKit

protocol OnTapGesture: AnyObject {
    func onTap(index: Int)
}
protocol Gesture {
    func addGesture(view: UIView,tag: Int)
}
class TapGesture: Gesture {
    weak private var callback: OnTapGesture?
    init(callback: OnTapGesture) {
        self.callback = callback
    }
    func addGesture(view: UIView,tag: Int) {
        view.isUserInteractionEnabled = true
        view.tag = tag
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(_:)))
        view.addGestureRecognizer(gesture)
    }
    @objc func viewTap(_ sender: UITapGestureRecognizer) {
        guard let index = sender.view?.tag else {
            return
        }
        callback?.onTap(index: index)
    }
    deinit {
        print("TapGesture is deinit.")
    }
}
