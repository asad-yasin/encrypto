//
//  AlertViewHnadler.swift
//  Solid
//
//  Created by Rana Asad on 22/09/2022.
//

import Foundation
import UIKit
protocol Alert {
    func showAlert(title: String,message: String,cancelButtonTitle: String,otherButtons: [String],onSelected: @escaping (Int) -> Void)
}
class AlertViewHandler {
    private let alert: Alert
    init(alert: Alert) {
        self.alert = alert
    }
    func showAlert(title: String, message: String,cancelButtonTitle: String,otherButtons: [String],onSelected: @escaping (Int) -> Void) {
        alert.showAlert(title: title, message: message, cancelButtonTitle: cancelButtonTitle, otherButtons: otherButtons, onSelected: onSelected)
    }
    deinit {
        print("AlertViewHandler is deinit.")
    }
    
}
class DefaultAlertView: Alert {
    private let controller: UIViewController
    private let style: UIAlertController.Style = .alert
    init(controller: UIViewController) {
        self.controller = controller
    }
    func showAlert(title: String, message: String,cancelButtonTitle: String ,otherButtons: [String],onSelected: @escaping (Int) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.destructive, handler: nil))
        otherButtons.forEach({ title in
            alert.addAction(UIAlertAction(title: title, style: UIAlertAction.Style.default, handler: { action in
                if let index = alert.actions.firstIndex(where: { $0 == action}) {
                    onSelected(index.advanced(by: 0))
                }
            }))
        })
        controller.present(alert, animated: true, completion: nil)
    }
    deinit {
        print("DefaultAlertView is deinit.")
    }
}
class DefaultAlertCustomFont: Alert {
    private let controller: UIViewController
    private let style: UIAlertController.Style = .alert
    private let fontName: String
    private let fontSize: CGFloat
    
    init(controller: UIViewController, fontName: String,fontSize: CGFloat) {
        self.controller = controller
        self.fontName = fontName
        self.fontSize = fontSize
    }
    
    func showAlert(title: String, message: String, cancelButtonTitle: String, otherButtons: [String], onSelected: @escaping (Int) -> Void) {
        let attrTitle = MutableStringHandler(mutableString: MutableStringCustomFont(fontName: fontName, fontSize: fontSize), str: title)
        let attrMessage = MutableStringHandler(mutableString: MutableStringCustomFont(fontName: fontName, fontSize: fontSize), str: message)
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: style)
        alert.setValue(attrTitle, forKey: "attributedTitle")
        alert.setValue(attrMessage, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.destructive, handler: nil))
        otherButtons.forEach({ title in
            alert.addAction(UIAlertAction(title: title, style: UIAlertAction.Style.default, handler: { action in
                if let index = alert.actions.firstIndex(where: { $0 == action}) {
                    onSelected(index.advanced(by: 0))
                }
            }))
        })
        controller.present(alert, animated: true, completion: nil)
    }
    deinit {
        print("DefaultAlertCustom is deinit.")
    }
}
