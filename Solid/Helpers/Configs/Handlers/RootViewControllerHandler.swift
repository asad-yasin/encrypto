//
//  RootViewControllerHandler.swift
//  Solid
//
//  Created by Rana Asad on 22/09/2022.
//

import Foundation
import UIKit
class RootViewControllerHandler {
    private let viewController: UIViewController
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    func showRootViewController() {
        if let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene {
            if let delegate = windowScene.delegate as? SceneDelegate {
                if let window = delegate.window {
                    window.rootViewController = viewController
                    window.makeKeyAndVisible()
                }
            }
        }
    }
}
