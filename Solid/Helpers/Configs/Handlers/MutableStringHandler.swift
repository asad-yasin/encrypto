//
//  MutableStringHandler.swift
//  Solid
//
//  Created by Rana Asad on 22/09/2022.
//

import Foundation
import UIKit
protocol MutableString {
    func getMutableString(str: String) -> NSMutableAttributedString
}
class MutableStringHandler {
    private let mutableString: MutableString
    private let str: String
    init(mutableString: MutableString,str: String) {
        self.mutableString = mutableString
        self.str = str
    }
    func getMutableString() -> NSMutableAttributedString {
        return mutableString.getMutableString(str: str)
    }
}
class MutableStringCustomFont: MutableString {
    private let fontName: String
    private let fontSize: CGFloat
    init(fontName: String, fontSize: CGFloat) {
        self.fontName = fontName
        self.fontSize = fontSize
    }
    func getMutableString(str: String) -> NSMutableAttributedString {
        let font = Font(fontName: fontName, fontSize: fontSize)
        let attrFont = [NSAttributedString.Key.font: font.getFont()]
        let attrString = NSMutableAttributedString(string: str, attributes:  attrFont)
        return attrString
    }
    
}
struct Font {
    let fontName: String
    let fontSize: CGFloat
    func getFont() -> UIFont {
        let font = UIFont(name: fontName, size: fontSize)
        guard let font = font else {
            return UIFont.systemFont(ofSize: fontSize)
        }
        return font
    }
}
