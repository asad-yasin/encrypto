//
//  Constants.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import Foundation
import UIKit

class Constants {
    
}
enum Fonts {
    public static let medium = "MADEOkineSansPERSONALUSE-Medium"
    public static let black = "MADEOkineSansPERSONALUSE-Black"
    public static let thin = "MADEOkineSansPERSONALUSE-Thin"
    public static let light = "MADEOkineSansPERSONALUSE-Light"
    public static let bold = "MADEOkineSansPERSONALUSE-Bold"
    public static let regular = "MADEOkineSansPERSONALUSE-Regular"
}
enum StringConstants {
    enum Alert {
        public static let confirmationTitle = "Confirm"
        public static let okButton = "Ok"
        public static let yesButton = "Yes"
        public static let noButton = "No"
        public static let cancelButton = "Cancel"
        public static let numberVerification = " is this your number?"
    }
}
class DecorationConstants {
    static let CORNER_RADIUS: CGFloat = 8
    static let ACCENT_COLOR = UIColor(named: "AccentColor")
    static let LEFT_PADDING: CGFloat = 8
}
