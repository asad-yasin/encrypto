//
//  User.swift
//  Solid
//
//  Created by Rana Asad on 22/09/2022.
//

import Foundation
// User protocol
protocol User {
    var id: String { get }
    var email: String { get }
    var phoneNumber: String { get }
}
class AppUser: User {
    var id: String
    var email: String
    var phoneNumber: String
    var firstname: String
    var lastname: String
    init(id: String,email: String,phoneNumber: String,firstname: String,lastname: String) {
        self.id = id
        self.email = email
        self.phoneNumber = phoneNumber
        self.firstname = firstname
        self.lastname = lastname
    }
}
class ChatUser: User {
    var id: String
    var email: String
    var phoneNumber: String
    init(id: String,email: String,phoneNumber: String) {
        self.id = id
        self.email = email
        self.phoneNumber = phoneNumber
    }
}
