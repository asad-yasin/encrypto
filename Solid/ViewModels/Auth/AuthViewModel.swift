//
//  AuthViewModel.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import Foundation
class AuthViewModel {
    public let viewDecorator: UIViewDecorator = UIViewDecorator()
    public let isLogin: Bool
    public var tapGesture: TapGesture?
    public var onTap: ((Int) -> ())?
    init(isLogin: Bool) {
        self.isLogin = isLogin
        tapGesture = TapGesture(callback: self)
    }
    deinit {
        print("AuthViewModel is deinit.")
    }
    
}
extension AuthViewModel: OnTapGesture {
    func onTap(index: Int) {
        onTap?(index)
    }
}
