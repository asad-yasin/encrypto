//
//  WelcomeScreenViewModel.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import Foundation
class SplashScreenViewModel {
    public let viewDecorator = UIViewDecorator()
    deinit {
        print("SplashScreenViewModel is deinit.")
    }
}
