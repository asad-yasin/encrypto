//
//  AuthViewController.swift
//  Solid
//
//  Created by Rana Asad on 21/09/2022.
//

import UIKit
import CountryPicker
class AuthViewController: UIViewController {
    // MARK: Outlets
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    // MARK: Varaibles
    private var viewModel: AuthViewModel?
    public var isLogin: Bool = false
    private var isNextEnabled: Bool = false {
        didSet {
            nextButtonState()
        }
    }
    // MARK: System Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        setupUI()
        addGesture()
        addDelegates()
        nextButtonState()
        // Do any additional setup after loading the view.
    }
    // MARK: User Define Methods
    public func configure() {
        self.viewModel = AuthViewModel(isLogin: isLogin)
    }
    // Add delegates to all view
    private func addDelegates() {
        numberTextField.delegate = self
    }
    // Adding the Animation according to the state of isNextEnabled.
    private func nextButtonState() {
        nextButton.isUserInteractionEnabled = isNextEnabled
            UIView.animate(withDuration: 1,animations: {[weak self] in
                guard let isEnabled = self?.isNextEnabled else {
                    return
                }
                self?.nextButton.alpha = isEnabled ? 1 : 0.6
            })
    }
    // Set Up the UI.
    private func setupUI() {
        guard let viewModel = viewModel else {
            return
        }
        // Adding the corner radius to views
        viewModel.viewDecorator.addCornerRadius(view: codeView, radius: DecorationConstants.CORNER_RADIUS)
        viewModel.viewDecorator.addCornerRadius(view: numberTextField, radius:DecorationConstants.CORNER_RADIUS)
        viewModel.viewDecorator.addCornerRadius(view: nextButton, radius: DecorationConstants.CORNER_RADIUS)
        // Adding the border color to views
        viewModel.viewDecorator.addBorderColor(view: numberTextField, color: DecorationConstants.ACCENT_COLOR)
        viewModel.viewDecorator.addBorderColor(view: codeView, color: DecorationConstants.ACCENT_COLOR)
        // Adding the Left Padding in TextFields
        numberTextField.setLeftPaddingPoints(DecorationConstants.LEFT_PADDING)
    }
    // Add the gesture to differenet views.
    private func addGesture() {
        guard let viewModel = viewModel else {
            return
        }
        guard let tapGesture = viewModel.tapGesture else {
            return
        }
        tapGesture.addGesture(view: codeView, tag: 1)
        viewModel.onTap = {[weak self] (index) in
            self?.showCountryPicker()
        }
    }
    private func showCountryPicker() {
        let countryPicker = CountryPickerViewController()
        countryPicker.selectedCountry = "PK"
        countryPicker.delegate = self
        self.present(countryPicker, animated: true)
    }
    private func verifyPhoneNumber() {
        guard let phoneNumber = numberTextField.text else {
            return
        }
        guard let code = codeLabel.text else {
            return
        }
        let fullNumber = code + phoneNumber
        showConfirmationAlert(phoneNumber: fullNumber)
        
    }
    private func showConfirmationAlert(phoneNumber: String) {
        let numberVerificationStr = phoneNumber + StringConstants.Alert.numberVerification
        let alertHandler = AlertViewHandler(alert: DefaultAlertView(controller: self))
        alertHandler.showAlert(title: StringConstants.Alert.confirmationTitle, message: numberVerificationStr, cancelButtonTitle: StringConstants.Alert.noButton, otherButtons: [StringConstants.Alert.yesButton], onSelected: {[weak self] (index) in
            switch index {
            case 1:
                self?.changeRootViewController()
                break
            default:
                break
            }
        })
    }
    private func changeRootViewController() {
        
        let rootViewController = RootViewControllerHandler(viewController: AppStoryboard.Main.viewController(viewControllerClass: HomeViewController.self))
        rootViewController.showRootViewController()
    }
    // MARK: Actions
    @IBAction func nextButtonClicked(_ sender: Any) {
        verifyPhoneNumber()
    }
    deinit {
        print("AuthViewController is deinit.")
    }
    
}
// MARK: Exetnsions
extension AuthViewController: CountryPickerDelegate {
    func countryPicker(didSelect country: Country) {
        codeLabel.text = "+" + country.phoneCode
    }
}
extension AuthViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else  {
            return false
        }
        if text.count > 4 {
            isNextEnabled = true
        } else {
            isNextEnabled = false
        }
        return true
    }
}
