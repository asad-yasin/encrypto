//
//  ViewController.swift
//  Solid
//
//  Created by Rana Asad on 20/09/2022.
//

import UIKit

class WelcomeScreenViewController: UIViewController {
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    private let viewModel: SplashScreenViewModel = SplashScreenViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    private func setupUI() {
        viewModel.viewDecorator.addCornerRadius(view: loginButton, radius: DecorationConstants.CORNER_RADIUS)
        viewModel.viewDecorator.addCornerRadius(view: signupButton, radius: DecorationConstants.CORNER_RADIUS)
        viewModel.viewDecorator.addBorderColor(view: loginButton, color: DecorationConstants.ACCENT_COLOR)
    }
    @IBAction func loginButtonClicked(_ sender: Any) {
        goToAuthController(isLogin: true)
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        goToAuthController(isLogin: false)
    }
    private func goToAuthController(isLogin: Bool) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: AuthViewController.self)
        vc.modalPresentationStyle = .fullScreen
        vc.isLogin = true
        self.present(vc, animated: true)
    }
    deinit {
        print("WelcomeScreenViewController is deinit.")
    }
    
}

